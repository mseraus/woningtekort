import {
  Values,
  Header,
  Hero,
  VideoSlider,
  Corporations,
  Footer,
} from "../Components";

const Home = () => (
  <>
    <Header />
    <Hero />
    <Values />
    <VideoSlider />
    <Corporations />
    <Footer />
  </>
);

export default Home;
