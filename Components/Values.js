const Values = () => (
  <section className="values">
    <div className="values__card">
      <h1 className="values__title ">
        Wij hebben ons altijd ingezet voor betaalbare woningen
      </h1>
      <p className="values__content">
        Dat is onze taak. Maar onder de huidige omstandigheden kunnen wij onze
        taak niet goed uitvoeren. Het gevolg: lange wachttijden, hoge huren en
        veel te weinig betaalbare woningen.
      </p>
    </div>
    <div className="values__card">
      <h1 className="values__title">
        Woningcorporaties kampen al jaren met geldtekort
      </h1>
      <p className="values__content">
        We kunnen nu minder dan nodig is investeren in nieuwbouw, renovatie,
        verduurzaming en leefbaarheid in wijken. Dat vinden wij net zo
        frustrerend als onze huurders en woningzoekenden die daar de ingrijpende
        gevolgen van ondervinden.
      </p>
    </div>
    <div className="values__card">
      <h1 className="values__title">
        den haag is aan zet, maar regio rotterdam net zo goed
      </h1>
      <p className="values__content">
        We willen dit geluid luid en duidelijk laten doordringen in politiek Den
        Haag. Maar ook bij de gemeentebesturen in de regio Rotterdam waarmee wij
        samenwerken. Betaalbare woningen zijn de komende jaren topprioriteit.
        Uiteindelijk wil iedereen gewoon wonen.
      </p>
    </div>
  </section>
);

export default Values;
