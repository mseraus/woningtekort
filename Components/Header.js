import Head from "next/head";
import Script from "next/script";

const Header = () => (
  <Head>
    <title>Gewoon Wonen</title>
    <meta name="Gewoon wonen nu" content="Woning Nu " />
    <link rel="icon" href="/favicon.ico" />
  </Head>
);

export default Header;
