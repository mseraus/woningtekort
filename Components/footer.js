const footer = () => (
  <footer className="footer">
    <p className="footer__title">
      <a target="_blank" href="/files/gewoonwonen.pdf">
        Download ons volledige statement
      </a>
    </p>
    <p className="footer__title">@Maaskoepel 2021</p>
  </footer>
);

export default footer;
