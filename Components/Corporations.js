const Corporations = () => (
  <section className="corporation">
    <div className="wrapper">
      <h1 className="corporation__title">
        MAASKOEPEL IS DE FEDERATIE VAN WONINGCORPORATIES IN DE REGIO ROTTERDAM
        EN ZET ZICH IN VOOR DE BELANGEN VAN ALLE WONINGZOEKENDEN IN DE REGIO
        ROTTERDAM. AL MEER DAN 25 JAAR. DIT DOEN WE NAMENS ÉN MET 22
        CORPORATIES, DIE SAMEN ZO'N 220.000 SOCIALE HUURWONINGEN BEHEREN.
      </h1>
      <ul className="corporation__list">
        <li className="corporation__item">
          <a
            href="https://www.3bwonen.nl"
            target="_blank"
            className="corporation__link"
          >
            3B Wonen
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.deleeuwvanputten.nl/"
            target="_blank"
            className="corporation__link"
          >
            De Leeuw van Putten
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.habion.nl/"
            target="_blank"
            className="corporation__link"
          >
            Habion
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.havensteder.nl/"
            target="_blank"
            className="corporation__link"
          >
            Havensteder
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.maasdelta.nl/"
            target="_blank"
            className="corporation__link"
          >
            Maasdelta Groep
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.maaswonen.nl/"
            target="_blank"
            className="corporation__link"
          >
            MaasWonen
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.mooiland.nl/"
            target="_blank"
            className="corporation__link"
          >
            Mooiland
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.patrimoniumbarendrecht.nl"
            target="_blank"
            className="corporation__link"
          >
            Patrimonium Barendrecht
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.quawonen.com/"
            target="_blank"
            className="corporation__link"
          >
            QuaWonen
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.ressortwonen.nl/"
            target="_blank"
            className="corporation__link"
          >
            Ressort Wonen
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.samenwerking.nl/"
            target="_blank"
            className="corporation__link"
          >
            Samenwerking Vlaardingen
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.sor.nl/"
            target="_blank"
            className="corporation__link"
          >
            SOR
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.vestia.nl/"
            target="_blank"
            className="corporation__link"
          >
            Vestia
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.waterwegwonen.nl/"
            target="_blank"
            className="corporation__link"
          >
            Waterweg Wonen
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.wvhwonen.nl/"
            target="_blank"
            className="corporation__link"
          >
            Wbv. Hoek van Holland
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://wbvpoortugaal.nl/"
            target="_blank"
            className="corporation__link"
          >
            Wbv. Poortugaal
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.woonbron.nl/"
            target="_blank"
            className="corporation__link"
          >
            Woonbron
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.wooncompas.nl/"
            target="_blank"
            className="corporation__link"
          >
            Wooncompas
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.woonplus.nl/"
            target="_blank"
            className="corporation__link"
          >
            Woonplus Schiedam
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.woonstadrotterdam.nl/"
            target="_blank"
            className="corporation__link"
          >
            Woonstad Rotterdam
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://dezeskernen.nl/"
            target="_blank"
            className="corporation__link"
          >
            Woonstichting De Zes Kernen
          </a>
          ,
        </li>
        <li className="corporation__item">
          <a
            href="https://www.woonzorg.nl/"
            target="_blank"
            className="corporation__link"
          >
            Woonzorg Nederland
          </a>
          ,
        </li>

        <li className="corporation__item">
          verenigd in&nbsp;
          <a
            href="http://www.maaskoepel.nl/"
            target="_blank"
            className="corporation__link"
          >
            Maaskoepel
          </a>
        </li>
      </ul>
    </div>
  </section>
);

export default Corporations;
