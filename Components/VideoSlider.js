import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import ReactPlayer from "react-player/youtube";
import { useState } from "react";

const VideoSlider = () => {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
      infinite: true,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  return (
    <section id="videos" className="slider">
      <h1 className="slider__title">
        wat er gezegd wordt <br /> over de wooncrisis
      </h1>
      <Carousel
        swipeable={true}
        partialVisbile={false}
        draggable={false}
        showDots={true}
        responsive={responsive}
        ssr={true} // means to render carousel on server-side.
        infinite={false}
        autoPlay={false}
        autoPlaySpeed={1000}
        keyBoardControl={true}
        transitionDuration={500}
        containerClass="carousel-container"
      >
        <div className="iframe__container">
          <ReactPlayer url="https://www.youtube.com/watch?v=-S5IopCV0Ro" />
        </div>
        <div className="iframe__container">
          <ReactPlayer url="https://www.youtube.com/watch?v=9jioHgSTZQo" />
        </div>
        <div className="iframe__container">
          <ReactPlayer url="https://www.youtube.com/watch?v=uEmLpOJXFSw" />
        </div>
      </Carousel>
    </section>
  );
};

export default VideoSlider;
