const HeroImage = () => (
  <figure className="hero__figure">
    <img className="hero__image" src="/hero-image.jpg" />
    <h1 className="hero__imageTitle">
      iedereen wil <br /> gewoon wonen
    </h1>
  </figure>
);

export default HeroImage;
