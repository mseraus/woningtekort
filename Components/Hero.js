import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import HeroImage from "./HeroImage";

const Hero = () => (
  <>
    <HeroImage />
    <div className="wrapper">
      <button className="hero__btn hero__btn--contact">
        <a href="mailto: communicatie@maaskoepel.nl ">
          <FontAwesomeIcon icon={faEnvelope} size="xs" />
        </a>
      </button>
      <div className="hero">
        <h1 className="hero__title">
          iedereen wil <br /> gewoon wonen
        </h1>

        <ul className="hero__list">
          <div className="hero__item">
            nauwelijks betaalbare woningen te vinden.
          </div>
          <div className="hero__item">
            wachtlijsten die alsmaar langer worden.
          </div>
          <div className="hero__item">Woningzoekenden zijn wanhopig.</div>
          <div className="hero__item">
            woonprotesten geven een duidelijk signaal.
          </div>
          <div className="hero__item">
            De nood is hoog. Juist in de regio Rotterdam.
          </div>
        </ul>

        <p className="hero__content">
          De 22 woningcorporaties in de regio Rotterdam zijn verenigd in de
          federatie Maaskoepel. Deze corporaties sympathiseren met de
          woonprotesten. Het is een duidelijk signaal dat inwoners steeds
          wanhopiger op zoek zijn naar een betaalbare woning.
        </p>

        <button className="hero__btn">
          <a href="#videos">
            <FontAwesomeIcon icon={faArrowDown} size="xs" />
          </a>
        </button>
      </div>
    </div>
  </>
);

export default Hero;
