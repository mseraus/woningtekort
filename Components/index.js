export { default as Header } from "./Header";
export { default as Hero } from "./Hero";
export { default as Values } from "./Values";
export { default as VideoSlider } from "./VideoSlider";
export { default as Corporations } from "./Corporations";
export { default as Footer } from "./footer";
